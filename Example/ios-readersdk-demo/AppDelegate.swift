//
//  AppDelegate.swift
//  ios-readersdk-demo
//
//  Created by Roger Serentill on 06/11/2019.
//  Copyright © 2019 Zinio. All rights reserved.
//

import UIKit
import ReaderSDK

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow?

  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
    _ = ZinioPro.initialize(
      with: Constant.zinioCredentials,
      configuration: ZinioProConfiguration.build()
        .bookmarksEnabled(true)
        .availableReaderModes([.pdf, .text])
        .newsstandId(Constant.newsstandId)
        .showTextToSpeech(false)
    )
    
    ZinioPro.shared.auth.signIn(with: Constant.userId)
    
    return true
  }
}

