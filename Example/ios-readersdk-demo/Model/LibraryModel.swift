//
//  LibraryModel.swift
//  ios-readersdk-demo
//
//  Created by Roger Serentill on 18/02/2020.
//  Copyright © 2020 Zinio. All rights reserved.
//

import Foundation

struct LibraryModel {
  let issueList = [
    Issue(
      imageURL: "https://cdn-assets.ziniopro.com/var/site_9856/storage/images/media2/cover11/29463-1-eng-US/cover2.jpg?t=ipad_portrait_cover",
      title: "September 2020",
      publication: "111 Linux Magazine",
      id: 449235,
      publicationId: 36358,
      hasPDF: true,
      hasXML: true
    ),
    Issue(
      imageURL: "https://cdn-assets.ziniopro.com/var/site_9856/storage/images/media2/cover11/29463-1-eng-US/cover2.jpg?t=ipad_portrait_cover",
      title: "September 2020",
      publication: "222 Linux Magazine",
      id: 465621,
      publicationId: 36358,
      hasPDF: true,
      hasXML: true
    ),
    Issue(
      imageURL: "https://cdn-assets.ziniopro.com/var/site_9856/storage/images/media2/cover11/29463-1-eng-US/cover2.jpg?t=ipad_portrait_cover",
      title: "September 2020",
      publication: "Linux Magazine",
      id: 489211,
      publicationId: 36358,
      hasPDF: true,
      hasXML: true
    ),
    Issue(
      imageURL: "https://cdn-assets.ziniopro.com/var/site_9973/storage/images/media2/cover5/3701-1-esl-ES/cover2.jpg?t=ipad_portrait_cover",
      title: "Agosto 2020",
      publication: "Class & Villas",
      id: 494795,
      publicationId: 37561,
      hasPDF: true,
      hasXML: true
    ),
    Issue(
      imageURL: "https://cdn-assets.ziniopro.com/var/site_1335/storage/images/media2/cover672/763811-1-eng-US/cover2.jpg?t=ipad_portrait_cover",
      title: "August 31, 2020",
      publication: "Us Weekly",
      id: 480760,
      publicationId: 3588,
      hasPDF: true,
      hasXML: true
    )
  ]
}

