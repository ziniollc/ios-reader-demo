//
//  Bookmark.swift
//  ios-readersdk-demo
//
//  Created by Roger Serentill on 14/02/2020.
//  Copyright © 2020 Zinio. All rights reserved.
//

import Foundation
import ReaderSDK

protocol Bookmark {
  var id: String { get }
  var issueName: String { get }
  var publicationName: String { get }
  var articleName: String? { get }
  var thumbnail: String? { get }
  var pageRange: String? { get }
}

extension StoryBookmark: Bookmark {
  var id: String { bookmarkId }
  var articleName: String? { title }
}

extension PageBookmark: Bookmark {
  var id: String { bookmarkId }
  var articleName: String? { title }
  var pageRange: String? { "" }
}
