//
//  Issue.swift
//  ios-readersdk-demo
//
//  Created by Samuel Hervás on 23/12/16.
//  Copyright © 2016 Zinio. All rights reserved.
//

struct Issue {
  let imageURL:String
  let title:String
  let publication:String
  let id: Int
  let publicationId: Int
  let hasPDF: Bool
  let hasXML: Bool
}
