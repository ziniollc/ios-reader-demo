//
//  UIImage+Download.swift
//  ios-readersdk-demo
//
//  Created by Samuel Hervás on 23/12/16.
//  Copyright © 2016 Zinio. All rights reserved.
//

import Foundation

import UIKit

extension UIImageView {
  func downloadedFrom(_ url: URL, contentMode mode: UIView.ContentMode = .scaleAspectFit) -> URLSessionDataTask {
    contentMode = mode
    
    let task = URLSession.shared.dataTask(with: url) { data, response, error in
      guard
        let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
        let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
        let data = data, error == nil,
        let image = UIImage(data: data)
        else { return }
      DispatchQueue.main.async { [weak self] in
        self?.image = image
      }
    }
    task.resume()
    
    return task
  }
  
  @discardableResult
  func downloadedFromLink(_ link: String, contentMode mode: UIView.ContentMode = .scaleAspectFit) -> URLSessionDataTask? {
    guard let url = URL(string: link) else { return nil }
    return downloadedFrom(url, contentMode: mode)
  }
}
