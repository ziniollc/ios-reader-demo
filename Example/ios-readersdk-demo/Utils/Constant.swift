//
//  Constant.swift
//  ios-readersdk-demo
//
//  Created by Jolles on 06/07/2018.
//  Copyright © 2018 Zinio LLC. All rights reserved.
//

import Foundation
import ReaderSDK

// NOTE: please, place your appropriate credentials
class Constant {
  static let userId = "USER_ID"
  static let clientId = "CLIENT_ID"
  static let clientSecret = "CLIENT_SECRET"
  static let apiVersion = "v2"
  static let projectId = 99
  static let applicationId = 9902
  static let newsstandId = 101
  
  static let zinioCredentials = ZinioProCredentials(
    clientId: clientId,
    clientSecret: clientSecret,
    projectId: projectId,
    applicationId: applicationId,
    environment: .sandbox(host: nil)
  )
}
