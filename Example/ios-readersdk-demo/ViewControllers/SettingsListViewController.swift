//
//  SettingsViewController.swift
//  ios-readersdk-demo
//
//  Created by Jolles on 15/11/2017.
//  Copyright © 2017 Zinio. All rights reserved.
//

import UIKit
import ReaderSDK

class SettingsListViewController: UIViewController {
  @IBOutlet open var tableView: UITableView!
}

extension SettingsListViewController: UITableViewDataSource {
  func numberOfSections(in tableView: UITableView) -> Int {
    1
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    4
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    var cell = UITableViewCell.init(style: .value1, reuseIdentifier: "SettingsCell")
    cell.selectionStyle = .none
    let switchView = UISwitch()
    switch (indexPath.row) {
      case 0:
        cell.textLabel?.text = "Download using Mobile Data"
        switchView.isOn = ZinioPro.shared.preferences.downloadUsingMobileData
        switchView.addTarget(self, action: #selector(actionWiFi), for: .valueChanged)
        cell.accessoryView = switchView
      case 1:
        cell.textLabel?.text = "Show thumbnails"
        switchView.isOn = ZinioPro.shared.preferences.showThumbnailsBar
        switchView.addTarget(self, action: #selector(actionThumbnails), for: .valueChanged)
        cell.accessoryView = switchView
      case 2:
        cell.textLabel?.text = "Show \"Article available\" popup"
        cell.accessoryView = switchView
      case 3:
        cell = UITableViewCell(style: .subtitle, reuseIdentifier: "PopupOptions")
        cell.textLabel?.text = "Reader mode"
        cell.detailTextLabel?.text = ZinioPro.shared.preferences.defaultReaderMode == .pdf ? "PDF" : "Text"
        cell.selectionStyle = .none
      default:
        break
    }
    return cell
  }
  
  @objc func actionWiFi(sender: Any) {
    ZinioPro.shared.preferences.downloadUsingMobileData = !ZinioPro.shared.preferences.downloadUsingMobileData
  }
  
  @objc func actionThumbnails(sender: Any) {
    ZinioPro.shared.preferences.showThumbnailsBar = !ZinioPro.shared.preferences.showThumbnailsBar
  }
  
  func actionReaderMode() {
    let actionPDF = UIAlertAction(title: "PDF", style: .default,
                                  handler: {[weak self] (_) in
                                    ZinioPro.shared.preferences.defaultReaderMode = .pdf
                                    self?.tableView.reloadData()
    })
    let actionText = UIAlertAction(title: "Text", style: .default,
                                   handler: {[weak self] (_) in
                                    ZinioPro.shared.preferences.defaultReaderMode = .text
                                    self?.tableView.reloadData()
    })
    showOptionsDialog(title: "Reader mode", message: nil, actions: actionPDF, actionText)
  }
  
  func showOptionsDialog(title: String?, message: String?, actions: UIAlertAction...) {
    let alert = UIAlertController(title: title, message: message,
                                  preferredStyle: .actionSheet)
    alert.view.tintColor = ZinioColors.neutralAccent
    for action in actions {
      alert.addAction(action)
    }
    
    if let popover = alert.popoverPresentationController {
      popover.sourceView = self.view
    }
    
    present(alert, animated: true, completion: nil)
  }
}

extension SettingsListViewController: UITableViewDelegate {
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    if indexPath.row == 3 {
      actionReaderMode()
    }
  }
}
