//
//  LibraryListViewController.swift
//  ios-readersdk-demo
//
//  Created by Jolles on 15/11/2017.
//  Copyright © 2017 Zinio. All rights reserved.
//

import UIKit
import ReaderSDK

class LibraryListViewController: UIViewController {
  private let model = LibraryModel()
  
  private var isReaderBusy = false
  
  // MARK: Layout elements
  
  @IBOutlet weak var collectionView: UICollectionView! {
    didSet {
      collectionView.register(.init(nibName: IssueCell.reuseIdentifier, bundle: nil),
                              forCellWithReuseIdentifier: IssueCell.reuseIdentifier)
    }
  }
  
  // MARK: View lifecycle
  
  override func viewDidLoad() {
    super.viewDidLoad()
    addObservers()
    #warning("Missing enableLogs in Demo App")
    //ZinioPro.shared.engine.enableLogs = true
  }
  
  // MARK: Actions
  
  @IBAction func didTapDeleteAll(_ sender: Any) {
    Task { @MainActor in
       _ = await ZinioPro.shared.content.removeAllData()
      self.collectionView.reloadData()
    }
  }
  
  @objc private func handleDownloadProgress(_ notification: Notification) {
    guard let issueProgress = notification.object as? IssueDownloadProgressNotification else { return }
    let issueId = issueProgress.issueId
    let percentage = issueProgress.progress.percentage
    print("Issue \(issueId) download progress: \(percentage * 100)%")
  }
}

// MARK: - UICollectionViewDataSource

extension LibraryListViewController: UICollectionViewDataSource {
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    model.issueList.count
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: IssueCell.reuseIdentifier,
                                                  for: indexPath) as! IssueCell
    cell.inject(delegate: self)
    return cell
  }
}

// MARK: - UICollectionViewDelegate

extension LibraryListViewController: UICollectionViewDelegate {
  
  func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
    guard let cell = cell as? IssueCell else { return }
    cell.display(issue: model.issueList[indexPath.row])
  }
  
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    guard !isReaderBusy else { return }
    isReaderBusy = true
    ZinioPro.shared.reader.openIssue(params: .init(issueId: model.issueList[indexPath.row].id), fromView: self) { [weak self]  result in
      switch result {
        case .success:
          self?.isReaderBusy = false
        case .failure(let error):
          self?.isReaderBusy = false
          print(self?.process(error: error) ?? "")
          print("***", error)
      }
    }
  }
}

// MARK: - IssueCollectionViewCellDelegate

extension LibraryListViewController: IssueCollectionViewCellDelegate {
  
  func cellDidTapRemoveButton(_ cell: IssueCell) {
    guard let indexPath = collectionView.indexPath(for: cell) else { return }
    
    let issueId = model.issueList[indexPath.row].id
    Task { @MainActor in
      let result = await ZinioPro.shared.content.deleteIssue(issueId: issueId)
      if result {
        self.collectionView.reloadItems(at: [indexPath])
      }
    }
  }
}

// MARK: - Private helpers

private extension LibraryListViewController {
  
  func addObservers() {
    NotificationCenter.default.addObserver(
      self,
      selector: #selector(handleDownloadProgress),
      name: .ReaderSDKIssueDownloadProgress,
      object: nil)
  }
  
  func process(error: Error) -> String {
    guard let error = error as? ZinioProError else { return "Unable to parse error"}
    switch error.type {
      case .sdkNotInitialized: return "Reader not initialised"
      case .networkError: return "There's no connectivity to open the issue"
      
      case .mobileDataDownloadNotAllowed(let value):
        return value.isEmpty ? "You're in mobile data network, download is not available. Please go to settings to change it." : value
      
      case .internalError: return "Internal error. Issue connot be opened"
      default: return "Cannot open issue"
    }
  }
}
