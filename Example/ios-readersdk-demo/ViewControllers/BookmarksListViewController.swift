//
//  BookmarksViewController.swift
//  ios-readersdk-demo
//
//  Created by Jolles on 15/11/2017.
//  Copyright © 2017 Zinio. All rights reserved.
//

import UIKit
import ReaderSDK

class BookmarksListViewController: UIViewController {
  @IBOutlet weak var collectionView: UICollectionView!
  @IBOutlet weak var segmentedControl: UISegmentedControl!
  
  var bookmarksList = [Bookmark]()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    configSegmentedControl()
    configCollectionViewLayout()
    
    self.collectionView.register(UINib.init(nibName:
      "BookmarkCell", bundle: nil), forCellWithReuseIdentifier: "BookmarkCell")
    
    reloadPageBookmarks()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    
    switch segmentedControl.selectedSegmentIndex {
      case 0: reloadPageBookmarks()
      case 1: reloadStoryBookmarks()
      default: break
    }
  }
  
  func configSegmentedControl() {
    segmentedControl.setTitle("Pages", forSegmentAt: 0)
    segmentedControl.setTitle("Stories", forSegmentAt: 1)
    segmentedControl.selectedSegmentIndex = 0
  }
  
  func configCollectionViewLayout() {
    let layout = UICollectionViewFlowLayout()
    layout.itemSize = CGSize(width: UIScreen.main.bounds.width, height: 115)
    layout.minimumLineSpacing = 0
    collectionView.setCollectionViewLayout(layout, animated: false)
    view.backgroundColor = .white
  }
  
  func reloadStoryBookmarks() {
    bookmarksList = ZinioPro.shared.content.getStoryBookmarks()
    collectionView.reloadData()
  }
  
  func reloadPageBookmarks() {
    bookmarksList = ZinioPro.shared.content.getPageBookmarks()
    collectionView.reloadData()
  }
  
  @IBAction func didTapDeleteAll(_ sender: Any) {
    switch segmentedControl.selectedSegmentIndex {
      case 0:
        if bookmarksList.count != 0, bookmarksList[0] is PageBookmark {
          Task { @MainActor in
            _ = await ZinioPro.shared.content.deletePageBookmarks(bookmarkIds: bookmarksList.map(\.id))
            reloadPageBookmarks()
          }
          
      }
      case 1:
        if bookmarksList.count != 0, bookmarksList[0] is StoryBookmark {
          Task { @MainActor in
            _ = await ZinioPro.shared.content.deleteStoryBookmarks(bookmarkIds: bookmarksList.map(\.id))
            reloadStoryBookmarks()
          }
      }
      default:
        break
    }
  }
  
  func processError(error: Error) -> String {
    var message = "Cannot open issue"
    if let err = error as? ZinioProError {
      switch (err.type) {
        case .sdkNotInitialized:
          message = "Reader not initialised"
        case .networkError:
          message = "There's no connectivity to open the issue"
        case .mobileDataDownloadNotAllowed(let mess):
          message = mess != "" ? mess : "You're in mobile data network, download is not available. Please go to settings to change it."
        case .internalError:
          message = "Internal error. Issue connot be opened"
        default:
          break
      }
    }
    return message
  }
  
  @IBAction func changeSection(_ sender: UISegmentedControl) {
    switch sender.selectedSegmentIndex {
      case 0:
        reloadPageBookmarks()
      case 1:
        reloadStoryBookmarks()
      default:
        break
    }
  }
}

// MARK: - UICollectionViewDataSource

extension BookmarksListViewController: UICollectionViewDataSource {
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    bookmarksList.count
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: BookmarkCell.reuseIdentifier, for: indexPath) as! BookmarkCell
    cell.inject(delegate: self)
    return cell
  }
}

// MARK: - UICollectionViewDelegate

extension BookmarksListViewController: UICollectionViewDelegate {
  
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    let bookmark = bookmarksList[indexPath.row]
    switch segmentedControl.selectedSegmentIndex {
      case 0:
        guard let bookmark = bookmark as? PageBookmark else { return }
        ZinioPro.shared.reader.openPageBookmark(params: .init(issueId: bookmark.issueId, page: bookmark.index), fromView: self) { result in
          switch result {
            case .success: break
            case .failure(let error):
              print(self.processError(error: error))
          }
        }
      case 1:
        guard let bookmark = bookmark as? StoryBookmark else { return }
        ZinioPro.shared.reader.openStoryBookmark(params: .init(issueId: bookmark.issueId, storyId: bookmark.storyId), fromView: self) { result in
          switch result {
            case .success: break
            case .failure(let error):
              print(self.processError(error: error))
          }
        }
      default:
        break
    }
  }
  
  func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
    guard let cell = cell as? BookmarkCell else { return }
    cell.display(bookmark: bookmarksList[indexPath.row])
  }
}

// MARK: - BookmarkCollectionViewCellDelegate

extension BookmarksListViewController: BookmarkCollectionViewCellDelegate {
  
  func didTapRemoveButton(cell: BookmarkCell) {
    if let indexPath = collectionView.indexPath(for: cell) {
      let bookmark = bookmarksList[indexPath.row]
      if let bookmark = bookmark as? PageBookmark {
        Task { @MainActor in
          _ = await ZinioPro.shared.content.deletePageBookmark(bookmarkId: bookmark.id)
          reloadPageBookmarks()
        }
        
      }
      else if let bookmark = bookmark as? StoryBookmark {
        Task { @MainActor in
          _ = await ZinioPro.shared.content.deleteStoryBookmark(bookmarkId: bookmark.id)
          reloadStoryBookmarks()
        }
      }
    }
  }
}
