//
//  BookmarkCollectionViewCell.swift
//  ios-readersdk-demo
//
//  Created by Jolles on 16/11/2017.
//  Copyright © 2017 Zinio. All rights reserved.
//

import UIKit

protocol BookmarkCollectionViewCellDelegate: AnyObject {
  func didTapRemoveButton(cell: BookmarkCell)
}

class BookmarkCell: UICollectionViewCell {
  
  // MARK: Layout elements
  
  @IBOutlet open var articleName: UILabel!
  @IBOutlet open var publicationName: UILabel!
  @IBOutlet open var issueName: UILabel!
  @IBOutlet weak var imageView: UIImageView!
  @IBOutlet weak var pageRangeLabel: UILabel!
  
  // MARK: Dependencies
  
  weak var delegate: BookmarkCollectionViewCellDelegate?
  
  private var imageDownloadTask: URLSessionDataTask?
  
  override func prepareForReuse() {
    super.prepareForReuse()
    imageDownloadTask?.cancel()
    imageView.image = nil
    articleName.text = nil
  }
  
  static var reuseIdentifier: String {
    String(describing: self)
  }
  
  func inject(delegate: BookmarkCollectionViewCellDelegate) {
    self.delegate = delegate
  }
  
  func display(bookmark: Bookmark) {
    issueName.text = bookmark.issueName
    publicationName.text = bookmark.publicationName
    articleName.text = bookmark.articleName
    if let thumbnail = bookmark.thumbnail, let url = URL(string: thumbnail) {
      imageDownloadTask = imageView.downloadedFrom(url)
    }
    else {
      imageView.image = nil
    }
    pageRangeLabel.text = bookmark.pageRange
  }
  
  // MARK: Actions
  
  @IBAction func didTapRemoveButton(_ sender: Any) {
    guard let delegate = delegate  else {
      return
    }
    
    delegate.didTapRemoveButton(cell: self)
  }
}
