//
//  CollectionViewCell.swift
//  ios-readersdk-demo
//
//  Created by Samuel Hervás on 26/9/16.
//  Copyright © 2016 Zinio. All rights reserved.
//

import UIKit

import ReaderSDK

protocol IssueCollectionViewCellDelegate: AnyObject {
  func cellDidTapRemoveButton(_ cell: IssueCell)
}

class IssueCell: UICollectionViewCell {
  @IBOutlet var coverImage: UIImageView!
  @IBOutlet var issueTitle: UILabel!
  @IBOutlet var issueName: UILabel!
  
  @IBOutlet var removeButton: UIButton!
  @IBOutlet var articleIcon: UIImageView!
  @IBOutlet var pdfIcon: UIImageView!
  @IBOutlet var issueProgressView: IssueProgressView!
  
  // MARK: Dependencies
  
  weak private var delegate: IssueCollectionViewCellDelegate?
  
  //MARK: Internal usage properties
  
  private var coverDownloadTask: URLSessionDataTask?
  
  // MARK: Object lifecycle
  
  override func awakeFromNib() {
    super.awakeFromNib()
    styleUI()
  }
  
  override func prepareForReuse() {
    super.prepareForReuse()
    coverDownloadTask?.cancel()
    coverImage.image = nil
    issueTitle.text = nil
  }
  
  static var reuseIdentifier: String {
    String(describing: self)
  }
  
  func inject(delegate: IssueCollectionViewCellDelegate) {
    self.delegate = delegate
  }
  
  func display(issue: Issue) {
    issueTitle.text = issue.title
    issueName.text = issue.publication
    if let url = URL(string: issue.imageURL) {
      coverDownloadTask = coverImage.downloadedFrom(url)
    }
    pdfIcon.isHidden = !issue.hasPDF
    articleIcon.isHidden = !issue.hasXML
    let progress = ZinioPro.shared.content.getIssueProgress(issueId: issue.id)
    removeButton.isHidden = progress.status == .none
    issueProgressView.startTracking(issueId: issue.id)
  }
  
  // MARK: Actions
  
  @IBAction func didTapDeleteIssue(_ sender: Any) {
    delegate?.cellDidTapRemoveButton(self)
  }
  
  // MARK: Helpers
  
  private func styleUI() {
    coverImage.layer.masksToBounds = false
    coverImage.layer.shadowRadius = 7.2
    coverImage.layer.shadowOpacity = 0.2
    coverImage.layer.shadowOffset = CGSize(width: 0, height: 2)
  }
}
