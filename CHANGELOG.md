# ReaderSDK
## iOS 
## Changelog

### Version 2.57.0
* Bug fixing and improvements
* This version is compiled with Xcode 15.2 and Swift 5.7.2

### Version 2.56.0
* Remove RXSwift dependency
* Bug fixing and improvements
* This version is compiled with Xcode 15.2 and Swift 5.7.2

### Version 2.55.0
* Bug fixing and improvements
* This version is compiled with Xcode 15.2 and Swift 5.7.2

### Version 2.54.0
* Bug fixing and improvements
* This version is compiled with Xcode 15.2 and Swift 5.7.2

### Version 2.53.0
* Bug fixing and improvements
* This version is compiled with Xcode 15.2 and Swift 5.7.2

### Version 2.52.0
* Migrate some functions to async/await
* Replace Zip dependency and use ZipFoundation with 0.9.16
* Bug fixing and improvements
* This version is compiled with Xcode 15.2 and Swift 5.7.2

### Version 2.51.0
* Updated Realm dependency version to match exact 10.44.0 version
* Bug fixing and improvements
* This version is compiled with Xcode 14.2 and Swift 5.7.2

### Version 2.50.0
* Updated KeychainAccess dependency version to match exact 4.2.2 version
* Updated RxRelay dependency version to match exact 6.5.0 version
* Bug fixing and improvements
* This version is compiled with Xcode 14.2 and Swift 5.7.2

### Version 2.49.0
* Updated KeychainAccess dependency version to match exact 4.2.2 version
* Updated RxRelay dependency version to match exact 6.5.0 version
* Bug fixing and improvements
* This version is compiled with Xcode 14.2 and Swift 5.7.2

### Version 2.48.0
* Bug fixing and improvements
* This version is compiled with Xcode 14.2 and Swift 5.7.2

### Version 2.47.0
* New feature: `Saved Articles` enables integrators the possibility to keep track of a list of articles, adding and removing them, or requesting the latest changes to the list from a given date
* Improvements: internal refactoring of models
* Bug fixing and improvements
* This version is compiled with Xcode 14.2 and Swift 5.7.2

### Version 2.46.0
* New feature: Added method in `ZinioProReader` to open a list of articles that enables changing between articles with swipe action
* Improvement: new methods in `ZinioProReader` to open an article, issue, last issue, and bookmarks
* Deprecate: methods from `ZinioProReader` to open an article, issue, last issue, and bookmarks. We recommend using the new ones
* Changes in types used in ZenithSDKProtocol properties
* Removed some RxSwift usages
* Documentation updates
* Bug fixing and improvements
* This version is compiled with Xcode 14.2 and Swift 5.7.2

### Version 2.45.0
* Bug fixing and improvements
* This version is compiled with Xcode 14.2 and Swift 5.7.2

### Version 2.44.1
* Updated Kingfisher dependency version to match exact 7.6.2 version
* This version is compiled with Xcode 14.2 and Swift 5.7.2

### Version 2.44.0
* Updated Realm dependency to 10.32.3
* Bug fixing and improvements
* Deprecated support for iOS 12
* This version is compiled with Xcode 14.2 and Swift 5.7.2

### Version 2.43.0

* Performance improvements in issue content rendering
* Fixed a crash when integrating ReaderSDK with CocoaPods
* Fixed a UI bug on the articles image gallery after rotating the device
* Improved Reader UI for devices with notch
* This version is compiled with Xcode 14.1 and Swift 5.7.1

### Version 2.42.4

* When opening an issue for the first time, prioritize cover download before other pages when in PDF mode
* Compiled with Xcode 13 and Swift 5.6

### Version 2.42.3

* Fixes a crash on iOS simulators when installing the ReaderSDK through SPM
* Compiled with Xcode 14 and Swift 5.7

### Version 2.42.2

* SPM bug fixes

### Version 2.42.1

* SPM bug fixes

### Version 2.42.0

* Added SPM Support
* Bug fixing and improvements

### Version 2.41.0
* Updated RxSwift version to 6.5.0
* Bug fixing and improvements

### Version 2.40.0
* Bug fixing and improvements

### Version 2.39.0
* Bug fixing and improvements

### Version 2.38.0
* `ZinioProCredentials` init parameters reordered into:
  * `clientId`
  * `clientSecret`
  * `projectId`
  * `applicationId`
  * `host`
* Bug fixing and improvements

### Version 2.37.0
* Bug fixing and improvements 

### Version 2.36.0
* Bug fixing and improvements 

### Version 2.35.0
* iOS deployment target updated to 12.0
* Bug fixing and improvements

### Version 2.34.0
* Bug fixing and improvements

### Version 2.33.0
* Bug fixing and improvements

### Version 2.32.0
* Bug fixing and improvements

### Version 2.31.0
* Bug fixing and improvements

### Version 2.30.0
* Deprecate 'Moya' framework dependency

### Version 2.29.0
* Bug fixing and improvements

### Version 2.28.0
* Bug fixing and improvements

### Version 2.27.0
* Bug fixing and improvements

### Version 2.26.0
* Bug fixing and improvements

### Version 2.25.0
* New feature: Search in magazine content
* Fixed UI bug with bookmark icon
* Optimized localization strings
* Fixed CoreGraphics crash affecting mostly iPad with iOS 12 and 11
* Minor bug fixes and improvements

### Version 2.24.0
* Bug fixing and improvements

### Version 2.23.0
* Add support for images embedded in templates
* Support whitelisted projects in SDK
* Update SDK Dependencies to Swift 5
* Fix "Share options" popup is misplaced after rotation
* Fix displaying conversion box whenever a user uses text tools

### Version 2.22.0
* Improve Reader performance and memory control
* Update project to iOS 11
* Update `RxSwift` dependency to v5.1.0
* Remove disabled features description from 'How to Navigate'
* Fix white flash during swiping in 'text' mode, in Reader.
* Fix rotation content misalignment
* Fix stuck in loading in issue cover in 'text' mode.

### Version 2.21.0
* Update `Moya`, `RxSwift` dependencies and remove unuseful as `AlamofireImage`, `Result` and `Haneke`.
* Update codebase from deprecated approaches.
* Update `ConversionBox` appearing, when content does not have area for scroll, after open.
* Fixed horizontal rotation for web articles via Reader.

### Version 2.20.0
* Bug fixing

### Version 2.19.0
* Performance improvement at PDF documents decrypting
* Correct page events tracking.

### Version 2.17.0
* Moved Latest News Reader inside Reader SDK
* Bookmarks animation
* Swift 5.1.2 compiler compatibility
* Bug fixing & improvements

### Version 2.16.1
* Bug fixing
* Bookmark sync enabled

### Version 2.15.0
* Bug fixing

### Version 2.14.0
* Bug fixing

### Version 2.13.0
* Bug fixing

### Version 2.12.2
* Bug fixing

### Version 2.12.1
* Bug fixing: Reader opening problems in iOS 9 and 10

### Version 2.12.0
* TOC redesign
* Ability to disable T2S
* Bug fixing

### Version 2.11.0
* Bug fixing

### Version 2.10.0
* Bug fixing
### Version 2.9.0
* Bug fixing

### Version 2.8.0
* Bug fixing

### Version 2.7.0
* New themes: Sepia and Grey
* Bug fixing

### Version 2.6.0
* Bug fixing

### Version 2.5.0
* Adding option to customise Last Page button/action
* New method to open the last magazine read by the user
* Bug fixing

### Version 2.4.5
* Fixed a bug related to Objective-C visibility of some methods
* Changed API base URL

### Version 2.4.4
* Reader text mode now uses CSS to render articles
* Increasing number of font size options
* Let user choose language on Text to Speech

### Version 2.3.0
* Bug fixing

### Version 2.2.0
* Bookmarks can now be kept when deleting an issue.
* Adds support for Xcode 10 and Swift 4.2

### Version: 2.1.0
* New Reader UX/UI
* Reader brightness
* Light/Dark mode applied to all the Reader
* Bug fixing

### Version: 2.0.0
* New Lightweight SDK interface
* Deprecation of old 1.x interface
* Content calls included inside SDK
* Authentication managed inside SDK
* PDF files security increased
* Authorisation and access to files managed and granted by the SDK
* See Readme document for more information about new SDK usage and migration from 1.x to 2.0 

### Version: 1.9.0
* *Bug fixing*

### Version: 1.8.0
* Analytics improvements
* Minor UI changes
* Manage Signed URL's 
* *Bug fixing*

### Version: 1.7.0
* Include vertical new DesignPack for Articles
* Analytics improvements
* Minor UI changes
* *Bug fixing*

### Version: 1.6.0
* Include vertical DesignPack for Articles
* Snackbar error not shown when there's an error on downloading file
* New analytics
* Adapt pending UI for iPhone X
* Thumbnails load improved
* Download issue in background
* UI improvements (Snackbar, Assets, ConversionBox, ...)
* *Bug fixing*

### Version: 1.5.1
* 1.5.0 compiled in XCode 9.3 with Swift 4.1 compatibility

### Version: 1.5.0
* Adapt UI for iPhone X
* New analytics
* Download speed increased
* DB access speed improved
* Delete process improved
* Thumbnails rendering improved
* *Bug fixing*

### Version: 1.4.1
* *Bug fixing*

### Version: 1.4.0
* Bookmarks splitted in StoryBookmarks & PageBookmarks to support PDF bookmarks
* Support Right to Left issues
* Add new async methods: deleteIssueAsync, removeAllDataAsync, downloadIssueAsync
* Download speed increased (multithread)
* Minor bug fixes

### Version: 1.3.0
* Permits downloading an issue with less priority than first one
* Swift 4 support
* Minor bug fixes

### Version: 1.2.1
* Fix a bug with one page articles in iOS 11
* Fix a bug that caused some articles to not load when changing orientation

### Version: 1.2.0
* Add ZinioAnalytics SDK as pod dependency of ZinioSDK. ZinioAnalytics track events, actions, screens and clicks on different engines analytics services.
* *Bug fixing*

### Version: 1.1.3
* Reader default mode option
* Change configuration from WiFi oriented to Mobile Data oriented
* Minor bugs fixed
* Performance improved

### Version: 1.1.2
* Improve network switching between 3G and WiFi
* Minor bugs fixed

### Version: 1.1.1
* Memory improvements
* Improvements in Reader experience
* Message informing 3G/WiFi connection when downloading
* Issue info functions added
* *Bug fixing*

### Version: 1.1.0
* Reset all ZinioSDK data
* Reset Config preferences
* Delete all issues
* Issues deletion is performed even if an issue is being currently downloaded 
* Performance of Reader in TXT mode improved
* *Bug fixing*

### Version: 1.0.9
* French and Dutch locales added in controls
* Conversion Box for Featured Articles
* Locales en,fr,de,it,nl added in default design pack (just used for external articles, not in issues. Issues download their own design pack and have their own translations)
* Empty views on Overview
* Add "Articles Available" pop-up option in ZinioSDKConfig

### Version: 1.0.8
* MOBILE-3867 Add Image Gallery to Reader SDK
* Customization of the reader primary color

### Version: 1.0.7
* Manage play-stop pause-resume states in download issue process
* *Bug fixing*

### Version: 1.0.6
* Add "show notifications" option in ZinioSDKConfig
* Add WiFi/MobileData downloads settings
* Bring back bottom Thumbnails bar
* Make bottom Thumbnails bar optional for SDK and for consumers
* Add to SDK the option User manages user preferences about download in 3G
* User opens Ad in Text Mode through link icon
* Control Reader initialization

### Version: 1.0.5
* Improve scroll smoth in SDK text mode
* *Bug fixing*

### Version: 1.0.4
* Concurrency issue some page never get loaded or downloaded
* Checking of allowXML, allowPDF, hasXML & hasPDF booleans
* *Bug fixing*

### Version: 1.0.3
* Make Bookmarks optional in the SDK
* *Bug fixing*

### Version: 1.0.2
* Open SDK option to enable/disable only download with WiFi.
* Added animation when showing reader.
* *Bug fixing*

### Version: 1.0.1
* Removed CryptoSwift dependency. No longer necessary.
* Refactoring. 
* Update SDK to get the entire design pack list
* MOBILE-2898 [Tablets only] PDF Reader: in the portrait the app's UI crosses the device's top navigation bar
* Info message on error.
* Changed background color while loading PDF.
* Restart the downloader from reachability when it was not paused
* User can see Download Status, reader shows an message when the download process has finished.
* *Bug fixing*

### Version 1.0.0
* First official Release

### Version: Alpha 1.0RC1
* Improvements in the Reader.
* Objective C Interface.

### Version: Alpha 1.0a
* Initial Pre-Release
