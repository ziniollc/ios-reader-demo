![Header](./Documentation/Header.png)



# iOS ReaderSDK

### Version 2.57.0

###### © COPYRIGHT 2001-2022 Zinio. ALL RIGHTS RESERVED.

  - [Description](#description)
  - [Installation](#installation)
    - [Requirements](#requirements)
    - [Swift Package Manager](#swift-package-manager)
    - [CocoaPods](#cocoapods)
  - [Usage](#usage)
    - [The Basics](#the-basics)
      - [Initilization](#initilization)
      - [Initial Configuration](#initial-configuration)
      - [Customization](#customization)
      - [Error Handling On Initialization](#error-handling-on-initialization)
    - [ZinioProError](#zinioproerror)
    - [ZinioPro Modules](#ziniopro-modules)
    - [ZinioProReader](#zinioproreader)
      - [Opening Issues](#opening-issues)
      - [Opening Articles](#opening-articles)
      - [Opening Article lists](#opening-articles-lists)
      - [Opening Bookmarks](#opening-bookmarks)
      - [Open External Content](#open-external-content)
      - [Analytics information](#analytics-information)
    - [ZinioProContent](#zinioprocontent)
      - [Download Issues](#download-issues)
      - [Cancel Downloads](#cancel-downloads)
      - [Retrieve Issue Information](#retrieve-issue-information)
      - [Retrieve Bookmarks Info](#retrieve-bookmarks-info)
      - [Remove Issues](#remove-issues)
      - [Remove Bookmarks](#remove-bookmarks)
      - [Remove All Data](#remove-all-data)
      - [Remove User Data](#remove-user-data)
    - [ZinioProAuth](#zinioproauth)
    - [ZinioProEngine](#zinioproengine)
    - [ZinioProPreferences](#ziniopropreferences)
    - [ZinioProAnalytics](#zinioproanalytics)
    - [ZinioProMigration](#ziniopromigration)
  - [Troubleshooting](#troubleshooting)
<br><br>

# Description


The iOS **ReaderSDK** is a powerful toolbox which provides users with a full experience to manage and read digital content (magazines, articles, etc). It also lets developers enrich such user experience by provinding a handset of interfaces to communicate with and configure the SDK.

Some use cases ReaderSDK satisfies:

* Open the Zinio Reader. A ready to use UI component fully implemented to provide the user with a great reading experience.
* Manage user's issues
* Get issue information
* Manage user's bookmarks
* Configure Zinio Reader
<br><br>

# Compatibility warning


We have begun the migration to Swift concurrency. Support will be provided for a limited number of versions going forward. Please make sure you update your code to make use of the new contract.
<br><br>



# Installation

## Requirements
* Xcode 15.2 or later
* Min iOS deployment target 13.0
* SDK client credentials (to be provided by Zinio)
<br><br>

## Swift Package Manager

Add `ReaderSDK` package into your project;

In Xcode, select `File` > `Add Packages...` and input the following URL:

```
https://bitbucket.org/ziniollc/ios-reader-sdk
```
<br>

## CocoaPods

You will fist need to add Zinio CocoaPods private repo to your CocoaPods installation:

```sh
pod repo add ZinioCocoaPods git@bitbucket.org:ziniollc/ziniococoapods.git
```

Then add the ReaderSDK pod to your `Podfile`:

```ruby
source 'https://cdn.cocoapods.org/'
source 'git@bitbucket.org:ziniollc/ziniococoapods.git'
 
target 'YOUR_TARGET_NAME' do
  use_frameworks! :linkage => :static
  pod 'ReaderSDK'
end
```

If you don't have a Podfile, run `pod init` on terminal inside your project directory to generate it.

Set **ENABLE BITCODE** to **NO**:
It might happen that a Bitcode related error occurs while compiling your project after pod installation. In that case, go to Build Settings in your project and set `Enable Bitcode` to `No`.
<br><br>

# Usage

## The Basics


### **Initilization**

`ZinioPro` exposes a singleton once it is initialized. It's important that it has been initialized before using it anywhere in the app, otherwise an exception will be throws, causing a runtime crash.

A good place to do so is during app startup, for example in `AppDelegate`'s `application(_:didFinishLaunchingWithOptions:)` method.

In order to initialize ZinioPro, call `initialize(with:)`, which requires a `ZinioProCredentials` parameter. Provide here your credentials so Zinio can recognize your application and allow its access.

```swift
let credentials = ZinioProCredentials(
  clientId: "YOUR-CLIENT-ID",
  clientSecret: "YOUR-CLIENT-SECRET",
  projectId: 0, // Your project id
  applicationId: 0, // your application id
  directoryId: 0, // your directory id
  environment: .sandbox(host: nil) // Optional, production is the default environment. `host` associated value is also value and it should normally be set to `nil`to use Zinio's default host.
)

_ = ZinioPro.initialize(with: credentials)
```


`ZinioProCredentials` Parameters

* `clientId` - Zinio client id
* `clientSecret` - Zinio client secret
* `projectId` - Zinio project id
* `applicationId` - Zinio application id
* `directoryId` - Zinio Directory id
* `environment` - (Optional) Zinio has two environments, `.sandbox(host:)` and `.production(host:)`. By default it will be set to production environment. If you have a specific host you want to point to, you must specify it as an associated value, otherwise, you can set `nil` to use Zinio's default host.
<br><br>

### **Initial Configuration**

You can also initialize ZinioPro with an optional `ZinioProConfiguration` parameter to setup a specific configuration, for exmample `availableReaderModes` or `enableBookmarks`. To do so, you need to initialize ZinioPro with `initialize(with: configuration:)` method.

The `ZinioProConfiguration` object follows the Builder pattern:

```swift
let configuration = ZinioProConfiguration.build()
  .enableBookmarks(true)
  .availableReaderModes([.pdf, .text])
  .newsstandId(1234)

_ = ZinioPro.initialize(with: credentials, configuration: configuration)
```


`ZinioProConfiguration` options:

* `defaultTheme(_:)` - Set Reader's default theme. App's current theme will be used if this function is not called (options: `.light`, `.sepia`, `.grey` or `.dark`).
* `bookmarksEnabled(_:)` - Enable or disable bookmarks feature
* `availableReaderModes(_:)` - The document formats to be available on the reader (options: `.pdf`, `.text`)
* `newsstandId(_:)` - Initial newsstand the SDK will use
* `tintColor(_:)` - Tint color for the reader
* `downloadUsingMobileData(_:)` - Allow or disallow to download content using Mobile data
* `showThumbnailsBar(_:)` - Enable or disable thumbnails bar within the reader while in PDF mode
* `defaultReaderMode(_:)` - The default reading mode when opening an issue for the first time (options: `.pdf`, `.text`)
* `showNotificationWhenIssueIsDownloaded(_:)` - Enable or disable showing a snackbar message when an issue download has completed
* `showTextToSpeech(_:)` - Enable or disable text to speech feature on the reader
* `meteredPaywallLinkText(_:)` - Set the CTA link's text to be displayed on the metered paywall (x out of Y articles left this month) componend inside the reader
* `storefrontContentRatingMax(_:)` - Set the maximum content rating value to filter unwanted content
* `savedArticlesFeatureDelegate(_ )` - Set savedArticlesDelegate to show a button when reading an article to save it. A backend and a database should be implemented by consumers.

<br><br>

### **Customization**

You can customize the colors of the Reader UI by setting `ZinioColors` static variables:
```swift
// These are the colors you might be interested in changing:

ZinioColors.accent // The corporative color used for highlited elements
ZinioColors.ctaBackground // Background color for CTA buttons
ZinioColors.ctaText // Text color for CTA butons
```
<br>

### **Error Handling On Initialization**

To have control over the initialitzation outcome, you can use `initialize(with: onSuccess: onError:)`:

```swift
ZinioPro.initialize(with: credentials, [configuration: configuration,] onSuccess: {
  // ReaderSDK initialized successfully
}) { error in
  // Handle ZinioProError error here
  error.type
  error.description
  error.extraInfo
}
```

<br>

## ZinioProError

* **`description`**: `String` - A description of the error
* **`code`**: `Int` - An error code for the error to be identified
* **`type`**: `ZinioProErrorType` - Enumerable with different type cases
* **`extraInfo`**: `[String: Any]` - Additional information about the error
<br><br>


| Code             | ZinioProErrorType              |
| ---------------- | ------------------------------ |
| 10               | `invalidCredentials`           |
| 20               | `accessDenied`                 |
| 30               | `missingNewsstand`             |
| 40               | `mobileDataDownloadNotAllowed` |
| 50               | `internalError`                |
| 60               | `contentError`                 |
| 70               | `sdkNotInitialized`            |
| 80               | `configurationError`           |
| HTTP status code | `apiError`                     |
| 408              | `networkError`                 |
| 0                | `unknown`                      |


<br>

## ZinioPro Modules

The entry point to ReaderSDK is `ZinioPro` class, which is a singleton providing access to seven main modules through `ZinioProSDK` protocol.

![ZinioPro](./Documentation/ZinioPro.png)

1. `ZinioProReader`: Used to open the Reader by issue, article, bookmarks, ...
2. `ZinioProContent`: Used to manage SDK contents. Download new content, get info from current content, remove and manage content, ...
3. `ZinioProPreferences`: Set SDK preferences: Download using Mobile data, show thumbnails bar in PDF mode, default opening mode, ...
4. `ZinioProAuth`: Manage user session.
5. `ZinioProAnalytics`: Used to track events, actions, etc inside the ReaderSDK. You can add your own trackers to capture and handle those events.
6. `ZinioProEngine`: Manage SDK internal engine.
7. `ZinioProMigration`: Offers the possibility to migrate content structure. Useful for major version releases.

Using `ZinioPro.shared` will allow you to use any of the above modules through `ZinioProSDK`:

```swift
public protocol ZinioProSDK {
  var reader: ZinioProReader { get }
  var content: ZinioProContent { get }
  var preferences: ZinioProPreferences { get }
  var auth: ZinioProAuth { get }
  var analytics: ZinioProAnalytics { get }
  var engine: ZinioProEngine { get }
  var migration: ZinioProMigration { get }
}
```

### **ReaderSDK**

The main modules are `ZinioProReader` and `ZinioProContent`, which are the ones responsible for displaying the user digital content. Here’s how the communication works:

<img src="./Documentation/ReaderSDK.png" alt="ReaderSDK" style="zoom:15%;" />

Note that all the modules aside from reader and content are left out of the picture, as they act as orchestrators in the background to manage needed configurations.

<br>

## ZinioProReader

<br>

This is the main module of ZinioPro. It allows you to open the reader UI with all its functionalities. It provides several methods to open issues, articles or bookmarks.

**Notes:**

Methods that open issues will automatically download the content before opening it if it hasn't been previously downloaded.

Remember that you need to set a `newsstandId` in order to grant the download.

If the content is already downloaded (or after a success download process), the Reader will be automatically opened.

<br>

### **Opening Issues**

To open an issue in the Reader you can use one of the following methods:

```swift
openIssue(params: fromView: completion:)
openLastIssue(params: fromView: completion:)
```

The second method will open the last magazine the user has read on the issue it was left.

* **`params`**: `ZinioProReaderOpenParams.Issue` - Parameters needed to open an Issue
* **`params`**: `ZinioProReaderOpenParams.LastIssue` - Parameters needed to open the last Issue
* **`fromView`**: `UIViewController` - The view controller on which the Reader will be presented
* *(optional)*  **`completion`**: `(Result<Void, Error>) -> Void` - Closure to be executed after the process finishes

**Input parameters**

They can be defined with:

```swift
enum ZinioProReaderOpenParams {
  struct Issue {
    let issueId: Int
    let lastPageCTA: LastPageCTA?
    let analyticsInfo: ReaderSDKData.AnalyticsInfo?
  }

  struct LastIssue {
    let analyticsInfo: ReaderSDKData.AnalyticsInfo?
  }
}
```

<br>

### **Opening Articles**

The reader can open a single article without downloading the content for the whole issue.

You have to provide a delegate that implements `ReaderArticleDelegate` protocol

You can also enable the **metered paywall** feature to engage users by letting them know how many freemium articles they have left.

To open an isolated article in the Reader, use the following method:

```swift
openArticle(params: fromView: completion: )
```

* **`params`**: `ZinioProReaderOpenParams.Article` - Parameters needed to open an Article
* **`fromView`**: `UIViewController` - The view controller on which the Reader will be presented
* *(optional)*  **`completion`**: `(Result<Void, ZinioProReaderOpenArticleError>) -> Void` - Closure to be executed after the process finishes

**Input parameters**

They can be defined with:

**`ZinioProReaderOpenParams.Article`**

```swift
enum ZinioProReaderOpenParams {
  struct Article {
    let ids: ReaderSDKData.ArticleIDsReferences
    let delegate: ReaderArticleDelegate
    let analyticsInfo: ReaderSDKData.AnalyticsInfo?
  }

  struct ArticleIDsReferences {
    let articleId: Int
    let issueId: Int
  }
}
```

<br>

### **Opening Articles lists**

The reader can open a lists of articles. Articles are downloaded on demand when navigating between articles from the list using the swipe action.

You have to provide a list of `ArticleIDsReferences` representing the ids required for each article (`articleId` and `issueId`). 

You have to pass a delegate that implements the `ReaderArticleListDelegate` protocol. When reaching the end of the list, you can load more elements onto the list of `ArticleIDsReferences` with the method callbacks from that protocol.

As in isolated articles, you can also enable the **metered paywall** feature to engage users by letting them know how many freemium articles they have left.

To open a list of articles, use the following method:

```swift
openArticles(params: fromView: completion:)
```

* **`params`**: `ZinioProReaderOpenParams.ArticlesList` - The parameters needed to open an Articles list
* **`fromView`**: `UIViewController` - The view controller on which the Reader will be presented
* *(optional)*  **`completion`**: `(Result<Void, ZinioProReaderOpenArticleListError>) -> Void` - Closure to be executed after the process finishes

**Input parameters**

The parameters used for opening the Article list can be defined with this:

```swift
enum ZinioProReaderOpenParams {
  struct ArticlesList {
    let data: ReaderSDKData.ArticleListInput
    let delegate: ReaderArticleListDelegate
    let analyticsInfo: ReaderSDKData.AnalyticsInfo?
  }
}

enum ReaderSDKData {
  struct ArticleListInput {
    let loadData: PagedArticleListLoadData
    let position: Int
  }

  struct PagedArticleListLoadData {
    let ids: [ArticleIDsReferences]
    let nextPage: NextPageIdentifier
  }
  
  enum NextPageIdentifier {
    case 
      none,
      value(Int)
      
    static func value(_ val: Int, if condition: @autoclosure () -> Bool) -> NextPageIdentifier
  }

  struct ArticleIDsReferences {
    let articleId: Int
    let issueId: Int
  }
}
```

<br>

### **Opening Bookmarks**

The reader can algo open an issue from a bookmarked page (PDF) or article (text). You can do so using the following methods:

```swift
openPageBookmark(params: fromView: completion:)
openStoryBookmark(params: fromView: completion:)
```

* **`params`**: `ZinioProReaderOpenParams.IssuePageBookmark` - Parameters used to open a page bookmark
* **`params`**: `ZinioProReaderOpenParams.IssueStoryBookmark` - Parameters used to open a story bookmark
* **`fromView`**: `UIViewController` - The view controller on which the Reader will be presented
* *(optional)*  **`completion`**: `(Result<Void, Error>) -> Void` - Closure to be executed after the process finishes

**Input parameters**

The parameters used for opening bookmarks can be defined with these models:

```swift
enum ZinioProReaderOpenParams {
  struct IssuePageBookmark {
    let issueId: Int
    let page: Int
    let lastPageCTA: LastPageCTA?
    let analyticsInfo: ReaderSDKData.AnalyticsInfo?
  }
  
  struct IssueStoryBookmark {
    let issueId: Int
    let storyId: Int
    let lastPageCTA: LastPageCTA?
    let analyticsInfo: ReaderSDKData.AnalyticsInfo?
  }
}
```

<br>

### **Open External Content**

ZinioPro also offers the possibility to open the reader and render an external article content within a web view. If you have an article as static content on a website and you want to use ZinioPro reader so the user can consume it directly in your app with the ZinioPro user experience, you will be able to do so with the following method:

```swift
openArticleFromURL(params: fromView: completion:)
```

* **`params`**: `ZinioProReaderOpenParams.ArticleURL` - Parameters needed to open an Article using a remote URL
* **`fromView`**: `UIViewController` - The view controller on which the Reader will be presented
* *(optional)*  **`completion`**: `(Result<Void, Error>) -> Void` - Closure to be executed after the process finishes

**Input parameters**

The parameters used for opening a remote Article can be defined with this:

```swift
enum ZinioProReaderOpenParams {
  struct ArticleURL {
    let url: URL
    let title: String
    let shareAction: WebShareAction?
    let analyticsInfo: ReaderSDKData.AnalyticsInfo?
  }
}
```

<br>

### **Analytics information**

When opening content, you can pass additional information to the Reader.

```swift
enum ReaderSDKData {
  struct AnalyticsInfo {
    let source: String
  }
}
```

<br>

## ZinioProContent

<br>
This module is responsible for providing the content. It retrieves and persists issues and articles content from Zinio backend service, as well as bookmarks information. It provides several methods to manage content.

### **Download Issues**

To download issue information (issue name, publication name, etc) and store it alongside its content into the SDK's local storage, the following methods are provided by the content module:

```swift
downloadIssue(issueId:) async -> Result<Issue, Error>
downloadIssue(issueId: priority:) async -> Result<Issue, Error>
```

* **`issueId`**: `Int` - Issue identifier
* **`priority`**: `QueuePriority` - Priority in the downloads queue (options: `.first`, `.last`)
* RETURN: Result of download operation

Both methods will first check if the issue is already downloaded and available on the reader's local storage, and if so, will directly return it without making any network request.

If the issue is not available locally, the download will be added to the download queue and wait its turn to be downloaded if `priority` is not defined or set to `last`. If `priority` is set to `first` the download will be immediately started.

### **Cancel Downloads**

The content module also allows you to cancel ongoing downloads:

```swift
cancelDownloadIssue(issueId:) async -> Bool
```

* **`issueId`**: `In`t - Issue identifier
* RETURN: Value if cancel was successful

This method, aside from cancelling any pending downloads for the specified issue, it will also remove any content previously downloaded associated to that issue, except its bookmarks.

### **Retrieve Issue Information**

To retrieve the information of the downloaded issues or pending to download, you can use the following methods:

```swift
getIssueInfo(issueId:) -> Issue?
getIssuesInfo() -> [Issue]
getLocalIssueCoverUrl(publicationId: issueId:) -> URL?
getPendingIssuesIds() -> Set<Int>
getDownloadedIssuesIds() -> Set<Int>
getAllIssuesIds() -> Set<Int>
getIssueProgress(issueId:) -> IssueProgress
getIssueSizeInMB(withIssueId issueId:) -> Int
```

**Parameters**

* **`publicationId`**: `Int` - Publication identifier
* **`issueId`**: `Int` - Issue identifier

**Returned Models**

**`Issue`**

```swift
class Issue {
  let issueId: Int
  let publicationId: Int
  let legacyIssueId: Int
  let name: String
  let cover: String
  let hasPDF: Bool
  let hasXML: Bool
  let allowPDF: Bool
  let allowXML: Bool
  let directionFlow: IssueDirectionFlow
  var publishDate: Date
  var consumerType: IssueConsumerType
  var consumerId: String
}

enum IssueDirectionFlow {
  case leftToRight, rightToLeft
}

enum IssueConsumerType {
  case user, device, external
}
```

**`IssueProgress`**

```swift
class IssueProgress {
  let status: IssueProgressStatus
  let percentage: Float
}

enum IssueProgressStatus {
  case none, downloading, paused, enqueued, finished, error
}
```

### **Retrieve Bookmarks Info**

Similarly, you can also retrieve info from all the bookmarks stored in the SDK. You can retrieve info for all story or page bookmarks or you can filter them by issue or publication:

```swift
getStoryBookmarks() -> [StoryBookmark]
getStoryBookmarks(publicationId:) -> [StoryBookmark]
getStoryBookmarks(issueId:) -> [StoryBookmark]

getPageBookmarks() -> [PageBookmark]
getPageBookmarks(publicationId:) -> [PageBookmark]
getPageBookmarks(issueId:) -> [PageBookmark]
```

**Parameters**

* **`publicationId`**: `Int` - Publication identifier
* **`issueId`**: `Int` - Issue identifier

**Returned Models**

**`StoryBookmark`**

```swift
class StoryBookmark {
  var bookmarkId: String
  let publicationId: Int
  let issueId: Int
  let storyId: Int
  var title: String
  var thumbnail: String?
  let section: String?
  var issueName: String
  var publicationName: String
  var issueCover: String?
  let pageRange: String?
  var modificationDate: Date?
  var publishDate: Date?
  var createdDate: Date
  var consumerType: IssueConsumerType
  var consumerId: String
  var fingerprint: String?
  var status: Int
  var synchronized: Bool
}

enum IssueConsumerType {
  case user, device, external
}
```

**`PageBookmark`**

```swift
class PageBookmark {
  var bookmarkId: String
  let publicationId: Int
  let issueId: Int
  var pageNumber: String
  var index: Int
  var title: String?
  var thumbnail: String?
  let section: String?
  var issueName: String
  var publicationName: String
  var issueCover: String?
  var modificationDate: Date?
  var publishDate: Date?
  var createdDate: Date
  var consumerType: IssueConsumerType
  var consumerId: String
  var fingerprint: String?
  var status: Int
  var synchronized: Bool
}

enum IssueConsumerType {
  case user, device, external
}
```

### **Remove Issues**

To remove previously downloaded issues (data and content) from the SDK's local storage, you can use the following methods:

```swift
deleteIssue(issueId:) async -> Bool
deleteIssue(issueId: [keepBookmarks:]) async -> Bool
deleteIssues(issuesIds:) async -> [Int]
deleteIssues(issuesIds: [keepBookmarks:]) async -> [Int]
deleteAllIssues() async -> Bool
deleteAllIssues([keepBookmarks:]) async -> Bool
```

* **`issueId`**: `Int` - The identifier of the issue to be removed
* **`issueIds`**: `Int` - Array of identifiers of issues to be removed
* *(optional)* **`keepBookmarks`**: `Bool` - A boolean to indicate if bookmarks should be kept or removed. Default option is `false`
* RETURN: Result of delete operation

### **Remove Bookmarks**

The content module also allows you to remove stored issue bookmarks in both text and pdf mode. We refer to text pages as *stories* and to pdf pages as *page*. The following methods are provided:

```swift
deleteStoryBookmark(bookmarkId:)
deleteStoryBookmark(bookmarkIds:)
deletePageBookmark(bookmarkId:)
deletePageBookmark(bookmarkIds:)
```

* **`bookmarkId`**: `String` - Bookmark identifier
* **`bookmarkIds`**: `[String]` - Array of bookmark identifiers

### **Remove All Data**

The following method removes all the SDK's local content:

```swift
removeUserData() async -> Bool
```

### **Remove User Data**

The following method removes all the SDK's local content associated to the current user:

```swift
removeUserData() async -> Bool
```

***Note:*** This method is automatically called when signing the user out by calling `ZinioPro.shared.auth.signOut()`. See `ZinioProAuth` for more information.

## ZinioProAuth

In order to identify the SDK with a Zinio user and let the SDK handle authorization when requesting protected resources (issues, articles, etc) use the following methods to save a user to the SDK:

```swift
signIn(with userId:)
signOut() async -> Bool
```

* **`userId`**: Zinio user identifier

Some functions are still available without being logged, for example some Articles (featured articles) are free and open to everybody.


## ZinioProEngine

ZinioPro has a downloader engine which is responsible for managing the downloads. It uses download queues to ensure all requested downloads are handled appropriately.

By default, the SDK resumes the downloader engine (taking into account preferences limitations) when it's initialized.

You can either start or stop the downloader engine using the following methods:

```swift
resumeDownloader()
stopDownloader()
```

In addition, you can track a download progress by observing the `ReaderSDKIssueDownloadProgress` notification to the default notification center, as in the following example:

```swift
// SomeViewController.swift

private func addObservers() {
  NotificationCenter.default
    .addObserver(
      self,
      selector: #selector(handleDownloadProgress),
      name: .ReaderSDKIssueDownloadProgress,
      object: nil
    )
}

@objc private func handleDownloadProgress(_ notification: Notification) {
  guard let issueProgress = notification.object as? IssueDownloadProgressNotification else { return }
  
  let issueId = issueProgress.issueId // The id of the issue we're downloading
  let progress = issueProgress.progress // Instance of IssueProgress
  let percentage = progress.percentage // Download progress from 0.0 to 1.0
  let status = progress.status // Status of the download (none, downloading, paused, enqueued, finished, error)
}
```

## ZinioProPreferences

This module lets you retrieve and/or change at runtime the preferences that where set up as the initial ZinioPro configuration. These are the available properties and methds:

```swift
var defaultTheme: Reader.ViewTheme { get set }
var bookmarksEnabled: Bool { get }
var availableReaderModes: [Int] { get }
var showTextToSpeech: Bool { get }
var meteredPaywallLinkText: String? { get }

var downloadUsingMobileData: Bool { get set }
var showThumbnailsBar: Bool { get set }
var defaultReaderMode: ReaderMode { get set }
var showNotificationWhenIssueIsDownloaded: Bool { get set }
var newsstandId: Int { get set }

allowsReaderMode(_ mode: AvailableReaderMode) -> Bool
```

You can refer to **Initial Configuration** section for more detailed information.

## ZinioProAnalytics

ReaderSDK triggers events when specific things happen while the app is running for analytics purposes. You can integrate your own analytics trackers to listen to and handle ReaderSDK events.

We separate events in four types:

* **Event:** Something relevant occurs in the app
* **Screen:** When a new screen shows up (normally the Reader screen)
* **Click:** When the user taps on specific interactive elements within the app
* **Action:** A specific action happened, normally done by the user, like taking a screenshot

To setup your trackers, create a class that conforms to `TrackerProtocol`. For this, your class will need to implement the following methods:

```swift
func track(event: String, parameters: [String: Any]?)

func track(screen: String, parameters: [String: Any]?)
  
func track(click: String, parameters: [String: Any]?)
  
func track(action: String, parameters: [String: Any]?)
```

Here is an example for a custom tracker called FancyTracker:

```swift
class FancyTracker: TrackerProtocol {
  func track(event: String, parameters: [String: Any]?) {
     // Send an event with its parameters to your analytics tool.
     ThirdPartyTracker.sendEvent(event, parameters: parameters)
  }
  
  func track(screen: String, parameters: [String: Any]?) {
    // Send a screen event with its parameters to your analytics tool.
    ThirdPartyTracker.sendEvent(screen, parameters: parameters)
  }
  
  func track(click: String, parameters: [String: Any]?) {
    // Send a click event with its parameters to your analytics tool.
    ThirdPartyTracker.sendEvent(click, parameters: parameters)
  }
  
  func track(action: String, parameters: [String: Any]?) {
    // Send an action event with its parameters to your analytics tool.
    ThirdPartyTracker.sendEvent(action, parameters: parameters)
  }
}
```

Then you just need to add your tracker to `ZinioProAnalytics` module with this method. Make sure to do so after `ZinioPro` has been initialized:

```swift
addAnalyticsTrackers(trackers:)
```

**Example:**

```swift
let tracker = FancyTracker()
ZinioPro.shared.analytics.addAnalyticsTrackers(trackers: [tracker])
```

If you need to set default parameters (parameters to be automatically sent alongside with every event), you can do so using the following method:

```swift
upsertDefaultParameters(_:)
```

**Example:**

```swift
let defaultParameters: [SDKTrackerParameters: Any] = [
  .userId: "some id",
  .deviceType: UIDevice.current.userInterfaceIdiom == .phone ? "iPhone" : "iPad"
]

ZinioPro.shared.analytics.upsertDefaultParameters(defaultParameters)
```

You can have as many trackers as you need, for example one tracker for every analytics tool you use. Just add them to the analytics module and ReaderSDK will invoke its methods when events are triggered.

## ZinioProMigration

This module is available in case any data structure or content in the reader SDK needs to be migration upon new versions. It usually won't be used unless explicitly requested by Zinio.

It contains legacy methods which were needed in past versions:

```swift
migratePublishDateToIssuesAndBookmarks(publishDate: issueId:)
migrateOwnerToIssuesAndBookmarks(consumerType: consumerId: issueId:)
```

You can safely ignore this module for now.

## Troubleshooting

We will provide solutions and workarounds as we find issues in our library

Contact
-------
[ziniomobileBCN@zinio.com](mailto:ziniomobileBCN@zinio.com)

